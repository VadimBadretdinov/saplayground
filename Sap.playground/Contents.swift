import UIKit

extension String {
    func getRangeOfString(position: Int) -> Range<String.Index>? {
        guard self.count > position else { return nil }
        let separator = "\n"
        let separatorCount = separator.count

        let array = self.components(separatedBy: separator)

        var count = 0
        if let value = array.lazy.filter({ (string) -> Bool in
            if array.first == string {
                count += string.count
            } else {
                count += string.count + separatorCount
            }
            return count >= position
        }).first {
            return self.range(of: value)
        } else {
            return nil
        }
    }
}

let str = "Hello, \n World\n Hello\nH\nE\nLLo"
if let range = str.getRangeOfString(position: 28) {
    let sttr = str[range.lowerBound..<range.upperBound]
    print(sttr)
}

